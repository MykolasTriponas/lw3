#include <avr/io.h>
#include <util/delay.h>
#include <stdint.h> 
#include <avr/interrupt.h>

volatile uint8_t overflow;



void timer0_init()
{
  TCCR0B|=(1<<CS02);
  TCNT0=0;
  TIMSK0|=(1<<TOIE0);
  sei();
}

void timer1_init()
{
  TCCR1A|=(1<<WGM11)|(1<<WGM10);
  TCCR1B|=(1<WGM13)|(1<<WGM12);
  TCCR1B|=(1<<CS12)|(1<<CS10);
  OCR1A=10000;
  TCNT1=0;
  TIMSK1|=(1<<OCIE1A);
  sei();
}

ISR(TIMER0_OVF_vect)
{
  overflow++;
}

ISR(TIMER1_COMPA_vect)
{
  PORTB^=(1<<3);
}

int main(void){
  DDRB|=(1<<2)|(1<<3);
  timer0_init();
  timer1_init();
  Serial.begin(1000);
  while(1)
  {
    if(overflow>=50)
    {
      if(TCNT0>=1)
      {
        PORTB ^=(1<<2);
        TCNT0=0;
        overflow=0;
      }
    }
  }
}